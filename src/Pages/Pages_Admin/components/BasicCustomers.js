import React, {useEffect, useState} from 'react'
import {db} from '../../../firebase';
import Clients from './Clients';

export default function BasicCustomers() {

    const [clients, setClients] = useState([]);

    useEffect(()=>{
        db.collection('2500').orderBy('timestamp', 'desc')
        .onSnapshot((snapshot)=>
        setClients(
            snapshot.docs.map((doc)=>({
                id:doc.id,
                identity: doc.data(),
            }))
        ));
    }, []);

  return (
    <div className="flex flex-col">

        {
            clients.map(({id, identity})=>(
                <Clients
                key = {id}
                Id = {id}
                identity = {identity.identity}
                lead = {identity.lead}
                paymentMethod = {identity.paymentMethod}
                timestamp = {identity.timestamp}
                guestNum={identity.guestNum}
                amount={identity.amount}
                />
            ))
        }

        
    </div>
  )
}
