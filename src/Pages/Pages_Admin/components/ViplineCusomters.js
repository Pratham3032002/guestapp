import React, {useEffect, useState} from 'react'
import {db} from '../../../firebase';
import Clients2 from './Clients2';

export default function ViplineCustomers() {

    const [clients, setClients] = useState([]);

    useEffect(()=>{
        db.collection('4000')
        .onSnapshot((snapshot)=>
        setClients(
            snapshot.docs.map((doc)=>({
                id:doc.id,
                identity: doc.data(),
            }))
        ));
    }, []);

  return (
    <div>

        {
            clients.map(({id, identity})=>(
                <Clients2
                key = {id}
                Id = {id}
                identity2 = {identity.identity}
                lead2 = {identity.lead}
                paymentMethod2 = {identity.paymentMethod}
                timestamp2 = {identity.timestamp}
                guestNum2={identity.guestNum}
                amount2={identity.amount}
                />
            ))
        }
    </div>
  )
}
