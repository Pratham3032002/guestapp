import React, { useCallback, useEffect, useState } from 'react'
import "./style.css"
import { useNavigate } from 'react-router-dom';
import TopHeader from '../../components/TopHeader';
import { getApicall } from '../../Network/ApiCall';
import { LIST } from '../../Network/baseUrl';
import Loader from '../../utils/Loader';
import Footer from '../../components/Footers';

const BarList = () => {
    const [loading, setLoading] = useState(false)
    const [getList, setGetList] = useState([])
    const navigate = useNavigate()

    const sucessSignUpCallback = useCallback(async (response) => {
        console.log("barList == ", response.data)
        setGetList(response.data)
        setLoading(false);
    });

    const errorSignUpCallback = useCallback(async (message) => {
        setLoading(false);
    });

    const barData = () => {
        setLoading(true)
        const url = LIST
        getApicall(url, sucessSignUpCallback, errorSignUpCallback)

    }
    
    let userData = JSON.parse(localStorage.getItem("data"))
    useEffect(() => {
        barData();
    }, [])

    const handleList = (list) => {
        localStorage.setItem("barID", list._id)
        navigate(`/bar-list/whisky-samba/${list._id }`)

    }
    localStorage.setItem("approved" , true)
    return (
        <div className="back_icon flex flex-col items-center h-screen py-4">
            <TopHeader username={userData.name} />

            <div className="barList">
            {
                loading ? <Loader /> :
                    getList?.map((list, index) => {
                        const { business_name, address, telephone, email } = list;
                        return (
                            <div className="bar_detail py-2 mb-4 mt-4" key={index} onClick={() => handleList(list)}>
                                <p className=" text-2xl font-bold" >{business_name}</p>
                                <p className="text-lg ">{address}</p>
                                <p className="text-lg ">{email}, {telephone}</p>
                            </div>
                        )
                    })
            }
            </div>
            <Footer />
        </div>
    )
}

export default BarList