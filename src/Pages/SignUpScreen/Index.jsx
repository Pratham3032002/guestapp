import React from 'react'
import "./style.css"
import Button from '../../components/Button/Button'
import { Link, useNavigate } from 'react-router-dom'

const SignUp = () => {
    const navigate = useNavigate()

    const clickHandler = () => {
        navigate("/signup")
    }
    localStorage.removeItem("auth")
    localStorage.removeItem("approved")

    return (
        <div className="h-screen w-screen flex flex-col items-center py-30" >
            <div className="flex flex-col items-center my-10">
                <div className=" text-7xl">Krunos</div>
                <div className=" mb-28">Buy Access. Buy Time</div>
                <div className="flex flex-col items-center">

                    <Button btnText='Connect with Twitter'
                        className="twitter_btnBg p-2 hover:font bold hover:text-black hover:bg-white w-60 bg-rose-600 text-xl text-white rounded-full shadow-lg mb-2" />
                    <Button btnText='Connect with Instagram'
                        className="insta_btnBg p-2 hover:font bold hover:text-black hover:bg-white w-60 bg-rose-600 text-ml text-white rounded-full shadow-lg mb-2" />
                    <Button btnText='Connect with LinkedIn'
                        className="linded_btnBg p-2 hover:font bold hover:text-black hover:bg-white w-60 bg-rose-600 text-xl text-white rounded-full shadow-lg mb-4" />
                </div>

                <div className="or_content mb-4">
                    <div className="hr_line"></div>
                    <div className="divider_text">or</div>
                    <div className="hr_line"></div>
                </div>

                <Button btnText='User Phone Mobile'
                    handleClick={clickHandler}
                    className="user_btnBg p-2 hover:font bold hover:text-black hover:bg-white w-60 bg-rose-600 text-xl text-white rounded-full shadow-lg mb-32" />
                {/* <img src={Twitter} className="mb-32" onClick={handleTwitter} /> */}
                <p className=" w-5/6 font-sm text-center">
                    By signing up, you agree to our{" "}
                    <Link to="/terms" className=" font-bold underline"> Terms. </Link>
                </p>
                <p className="w-5/6 font-sm text-center">
                    See how we use your data in our{" "}
                    <Link to="/privacy" className=" font-bold underline"> Privacy Policy </Link>
                </p>
            </div>
        </div>

    )
}

export default SignUp
