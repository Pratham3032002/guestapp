import axios from 'axios'
import { useFormik } from 'formik'
import React, { useState, useEffect, useCallback } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import Button from '../../components/Button/Button'
import Inputbox from '../../components/Input/Inputbox'
import { postApicallHead } from '../../Network/ApiCall'
import { ADD_USER } from '../../Network/baseUrl'
import { Detail_Schema } from '../../utils/validation'
import "./style.css"
import dummy_image from '../../assets/Add.png'

const ConfirmDetail = () => {

  const [loading, setLoading] = useState(false);
  const [image, setImage] = useState([]);
  const [UserImg, setUserImg] = useState("")
  const [errorText, setErrorText] = useState("")
  const navigate = useNavigate()

  // useEffect(() => {
  //   setNum(localStorage.getItem("number"))
  // }, [])
  const approved = localStorage.getItem("approved")
  useEffect(() => {
    if (approved) {
      navigate("/bar-list")
    }
  }, [])

  if (approved) {
    navigate("/bar-list")
  }


  const Intial = { name: '', email: '', date: '', num: localStorage.getItem("number") || "" }

  const { handleChange, handleSubmit, values, errors, touched } = useFormik({
    initialValues: Intial,
    validationSchema: Detail_Schema,
    onSubmit: values => {
      console.log(values);
      return (
        handleDetail(values)
      )
    }
  });


  const handleDone = (e) => {
    console.log("ddksldkl", errors)
    e.preventDefault()
    handleSubmit();
  }

  // const changeValue = (e) => {
  //   e.preventDefault()

  //   setData({
  //     ...data,
  //     contact: num,
  //     [e.target.name]: e.target.value,
  //   });
  // };


  const handleChangeSocial = () => {

    console.log("iiiioi",);
  }

  const handleChoose = (e) => {
    console.log(e.target.files[0])
    setImage(e.target.files[0])
    setUserImg(URL.createObjectURL(e.target.files[0]));
  }

  const sucessSignUpCallback = useCallback(async (res) => {
    console.log(res, 'ckeck token')
    setLoading(false);
    const data = { name: values.name, email: values.email, date: values.date, contact: values.num }
    localStorage.setItem("data", JSON.stringify(data));
    localStorage.setItem("userId", res.user._id);
    navigate("/bar-list");
  });

  const errorSignUpCallback = useCallback(async (message) => {
    setLoading(false);
  });



  const handleDetail = () => {
    setLoading(true);
    const formdata = new FormData();
    formdata.append("name", values.name)
    formdata.append("email", values.email)
    formdata.append("date", values.date)
    formdata.append("contact", values.num)
    formdata.append("file", image)
    const url = ADD_USER;
    const headers = {
      "Content-Type": "multipart/form-data"
  };
  postApicallHead(url, formdata, sucessSignUpCallback, errorSignUpCallback , headers)
    // else {
    //   setErrorText("This field is required")
    //   setLoading(false)
    // }

  }

  return (
    <div className="  w-screen flex flex-col items-center py-16" >
      <div className=" text-4xl text-center mb-4 px-4 font-base">Confirm Your Details</div>
      <div className=" confirmDetails font-bold flex flex-col justify-around items-center h-2/5 ">

        <Inputbox autoComplete="off" name="name" placeholder="John Doe" handleChange={handleChange} value={values.name}
          className="border w-64 h-12 tracking-widest text-xl bg-white text-center mb-2 " />

        <div className="flex w-screen items-center text-white justify-center">
          {
            errors.name && touched.name && <span className="text-rose-600 center mb-4">{errors.name}{touched.name}</span>
          }
        </div>
        <Inputbox autoComplete="off" name="email" placeholder="EMAIL " handleChange={handleChange} value={values.email}
          className="border w-64 h-12 tracking-widest text-xl bg-white text-center mb-2 "
        />
        <div className="flex w-screen items-center text-white justify-center">
          {
            errors.email && touched.email && <span className="text-rose-600 center mb-4">{errors.email}{touched.email}</span>
          }
        </div>
        <Inputbox
          autoComplete="off"
          className="border bg-gray-100	 w-64 h-12 tracking-widest text-xl bg-white text-center mb-2 "
          placeholder="NUMBER"
          // errors={errors.code && touched.code}
          disable={true}
          handleChange={handleChange}
          value={values.num} />
        <div className="">
        </div>
        <Inputbox type="date" name="date" placeholder="09/09/1992" handleChange={handleChange} value={values.date}
          className=" border w-64 border h-12 tracking-widest text-xl bg-white text-center mb-2  pr-4"
        />
        <div className="flex w-screen items-center text-white justify-center">
          {
            errors.date && touched.date && <span className="text-rose-600 center mb-4">{errors.date}{touched.date}</span>
          }
        </div>
      </div>
      <div>
        <div className="image_round my-4 flex flex-col items-center w-32">
          {/* <img src={UserImg} alt="" /> */}
           <label htmlFor="main"> {UserImg ? <img src={UserImg} alt="" /> : <img src={dummy_image} alt="" />} </label>
          <form action="" encType="multipart/form-data" className="mainas mt-2">
            <label htmlFor="main" className="p-2 hover:font bold hover:text-black hover:bg-white  bg-black text-xl text-white rounded-lg shadow-lg mt-8 cursor-pointer"> {UserImg ? "Change" : "Add"}</label>
            <input name="dis" id='main' type="file" className="" onChange={handleChoose} accept="image/*"
            />
          </form>
        </div>

      </div>
      {errorText && <div className='error'>{ errorText}</div>}
      <div>
        <Button
          btnText="Done"
          btntype="button"
          loading={loading}
          handleClick={handleDone}
          className="p-2 hover:font bold hover:text-black hover:bg-white w-52 bg-black text-xl text-white rounded-full shadow-lg mt-8 mb-2"
        />

      </div>
      <p className=" w-5/6 font-sm text-center">
        By signing up, you agree to our{" "}
        <Link to="/terms" className=" font-bold underline"> Terms. </Link>
      </p>
      <p className="e w-5/6 font-sm text-center">
        See how we use your data in our{" "}
        <Link to="/privacy" className=" font-bold underline"> Privacy Policy </Link>
      </p>
    </div>
  )
}

export default ConfirmDetail