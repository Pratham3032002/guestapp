import React from 'react'
import "../TicketType/style.css"
import StartRating from '../../../utils/StartRating'

const FeedbackPage = () => {
  return (
    <div className="flex flex-col items-center justify-center px-8 py-8 h-screen">
    <div className="h-2/5 flex items-center justify-center">
          <StartRating />
    </div>
    <div className="h-2/5">
      <p className=" font-bold text-3xl text-center">
        Standard FeedbackPage UI
      </p>
    <ul className="unorder text-sm">
      <li>Rate service</li>
      <li>Rate 'how buzzing was the place'</li>
      <li>Rate Food</li>
      <li>Additional comments</li>
    </ul>
    </div>
  </div>
  )
}

export default FeedbackPage