// import React, { useCallback, useEffect, useState } from 'react'
// import { useLocation, useNavigate, useParams } from 'react-router-dom'
// import Button from '../../../components/Button/Button'
// import { postApicall, puttApicall } from '../../../Network/ApiCall'
// import { BOOK_NOW, Imageurl, UPDATE_USER } from '../../../Network/baseUrl'
// import "../TicketType/style.css"
// let total
// const TicketDis = () => {
//   const [loading, setLoading] = useState(false)
//   const [isActive, setIsActive] = useState(false);
//   const [isActive2, setIsActive2] = useState(false);
//   const [isActive3, setIsActive3] = useState(false);
//   const [payment, setPayment] = useState("false");

//   const [paymentMethod , setPaymentMethod] = useState("")
//   const navigate = useNavigate()
//   const { state } = useLocation()
//   console.log("state", state)

//   console.log("state" , state.state.title)
//   const { id } = useParams()
//   const [memberCount, setmemberCount] = useState(1)
//   const total = (memberCount) * (state.value || state)
//   const barId = localStorage.getItem("barID")
//   const userId = localStorage.getItem("userId")
//   const handleClick1 = () => {
//     setPaymentMethod("Card")
//     setIsActive(true);
//     setIsActive2(false);
//     setIsActive3(false);


//   };

//   const handleClick2 = () => {
//     setPaymentMethod("Cash")
//     setIsActive2(true);
//     setIsActive3(false);
//     setIsActive(false);


//   };

//   const handleClick3 = () => {
//     setPaymentMethod("UPI")
//     setIsActive3(true);
//     setIsActive(false);
//     setIsActive2(false);
//   };
// // console.log(first)
//   const mainData = JSON.parse(localStorage.getItem("data"))
//   const MainPhoto = state.state.photo?.replace("uploads\\" , "")
//   const sucessSignUpCallback = useCallback( (res) => {
//     console.log(res)
//     setLoading(false);
//     navigate("/waiting-page")
//   });

//   const errorSignUpCallback = useCallback( (message) => {
//     setLoading(false);
//   });

//   const MinusPrice = useCallback(() => {
//     if (memberCount > 1) {
//       setmemberCount(memberCount - 1)
//     }
//   })
//   const PlusPrice = useCallback(() => {
//     setmemberCount(memberCount + 1)
//   })
//   const handleClick =  (e) => {
//     e.preventDefault();
//     if(paymentMethod){
//       const url = `${BOOK_NOW}/${barId}`
//       const obj = {userID:userId, access_price:state.state.seating, total_price: total, buy_admission:memberCount, paymentType: paymentMethod , userName:mainData.name}
      
//       const params = { booking_details: obj}
//        postApicall(url, params, sucessSignUpCallback, errorSignUpCallback)
//     }else{
//       alert("Please select payment method ")
//     }
//   }
//   return (
//     <div className="ticket_bg"  style={{
//       backgroundImage: `url(${Imageurl}/${MainPhoto})`
//     }}>
//       <div className="bg-white bottom_style">
//         <div className="py-8 px-8 dis_page">
//           <p className=" font-bold text-3xl mb-1">{state.state.title}</p>
//           <p>{state.state.business_name}</p>
//           <div className="access_p">
//             Access Price (1 Person) <span className="text-lg font-bold ml-1"> Rs.{state.value}</span>
//           </div>
//           <div className="access_p bg-rose-600 text-white">

//             By Admission <Button className="mx-2 px-2 font-bold" btnText="-" handleClick={MinusPrice} /> {memberCount} <Button className="mx-2 px-2 font-bold" btnText="+" handleClick={PlusPrice} />
//           </div>
//           <p className="font-bold text-lg">
//             What is Kronus<sup>TM</sup> about?
//           </p>
//           <p className="dis_text text-sm">
//             Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi officia possimus earum cumque recusandae expedita fugit ad minus,
//           </p>
//           <p className="font-bold">
//             Terms
//           </p>
//           <p className="dis_text text-sm">
//             Non Refundable 
//           </p>
//           <p className="dis_text text-sm">
//           100% Redeemable 
//           </p>
//           <p className='PaymentMode pt-3'>Please select payment mode</p>
//           <div className="flex justify-center pt-3 gap-4 text-md ">

//           <div style={{ background: isActive ? "#E11D48" : "black", color: isActive ? "white" : "white",  }}
//             onClick={handleClick1}
//             className="PaymentOption bg-black "
//           >
//             CARD
//           </div>
//           <div style={{ background: isActive2 ? "#E11D48" : "black", color: isActive2 ? "white" : "white",
//             }}
//             onClick={handleClick2}
//             className="PaymentOption bg-black "
//           >
//             CASH
//           </div>
//           <div style={{ background: isActive3 ? "#E11D48" : "black", color: isActive3 ? "white" : "white",
//             }}
//             onClick={handleClick3}
//             className="PaymentOption bg-black "
//           >
//             UPI
//           </div>
//         </div>
//         </div>
//         <div className="book_option px-8">
//           <div className="">
//             <p className="dis_text1 text-sm">
//               Total Price
//             </p>
//             <p className="font-bold text-2xl">
//               Rs.{total}
//             </p>
//           </div>
//           <Button btnText="Book Now"
//             handleClick={handleClick}
//             className="bg-rose-600 h-12 rounded-2xl w-28 text-white font-bold" />
//         </div>
//       </div>
//     </div>
//   )
// }

// export default TicketDis


import React, { useCallback, useEffect, useState } from 'react'
import { useLocation, useNavigate, useParams } from 'react-router-dom'
import Button from '../../../components/Button/Button'
import { postApicall, puttApicall } from '../../../Network/ApiCall'
import { BOOK_NOW, Imageurl, UPDATE_USER } from '../../../Network/baseUrl'
import "../TicketType/style.css"
let total
const TicketDis = () => {
  const [loading, setLoading] = useState(false)
  const [isActive, setIsActive] = useState(false);
  const [isActive2, setIsActive2] = useState(false);
  const [isActive3, setIsActive3] = useState(false);
  const [payment, setPayment] = useState("false");

  const [paymentMethod , setPaymentMethod] = useState("")
  const navigate = useNavigate()
  const { state } = useLocation()
  console.log("state", state)

  console.log("state" , state.state.title)
  const { id } = useParams()
  const [memberCount, setmemberCount] = useState(1)
  const total = (memberCount) * (state.value || state)
  const barId = localStorage.getItem("barID")
  const userId = localStorage.getItem("userId")

  const handleClick1 = () => {
    setPaymentMethod("Card")
    setIsActive(true);
    setIsActive2(false);
    setIsActive3(false);
  };

  const handleClick2 = () => {
    setPaymentMethod("Cash")
    setIsActive2(true);
    setIsActive3(false);
    setIsActive(false);


  };

  const handleClick3 = () => {
    setPaymentMethod("UPI")
    setIsActive3(true);
    setIsActive(false);
    setIsActive2(false);
  };

  const mainData = JSON.parse(localStorage.getItem("data"))
  const MainPhoto = state.state.photo?.replace("uploads\\" , "")


  const sucessSignUpCallback = useCallback(async (res) => {
    console.log(res)
    if(res.success){

      setLoading(false);
      navigate("/waiting-page")
      alert(res.message)
    } else {
      alert(res.message)
    }
  });

  const errorSignUpCallback = useCallback(async (message) => {
    setLoading(false);
  });

  const MinusPrice = useCallback(() => {
    if (memberCount > 1) {
      setmemberCount(memberCount - 1)
    }
  })
  const PlusPrice = useCallback(() => {
    setmemberCount(memberCount + 1)
  })
  const handleClick = async (e) => {
    e.preventDefault();
    if(paymentMethod){
      const url = `${BOOK_NOW}/${barId}`
      const obj = {userID:userId, access_price:state.state.seating, total_price: total, buy_admission:memberCount, paymentType: paymentMethod , userName:mainData.name, ticket_name:state.state.title} 

      const params = { booking_details: obj}
      await postApicall(url, params, sucessSignUpCallback, errorSignUpCallback)
    }else{
      alert("Please select payment method ")
    }
  }
  return (
    <div className="ticket_bg"  style={{
      backgroundImage: `url(${Imageurl}/${MainPhoto})`
    }}>
      <div className="bg-white bottom_style">
        <div className="py-8 px-8 dis_page">
          <p className=" font-bold text-3xl mb-1">{state.state.title}</p>
          <p>{state.state.business_name}</p>
          <div className="access_p">
            Access Price (1 Person) <span className="text-lg font-bold ml-1"> Rs.{state.value}</span>
          </div>
          <div className="access_p bg-rose-600 text-white">

            By Admission <Button className="mx-2 px-2 font-bold" btnText="-" handleClick={MinusPrice} /> {memberCount} <Button className="mx-2 px-2 font-bold" btnText="+" handleClick={PlusPrice} />
          </div>
          <p className="font-bold text-lg">
            What is Kronus<sup>TM</sup> about?
          </p>
          <p className="dis_text text-sm">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi officia possimus earum cumque recusandae expedita fugit ad minus,
          </p>
          <p className="font-bold">
            Terms
          </p>
          <p className="dis_text text-sm">
            Non Refundable 
          </p>
          <p className="dis_text text-sm">
          100% Redeemable 
          </p>
          <p className='PaymentMode pt-3'>Please select payment mode</p>
          <div className="flex justify-center pt-3 gap-4 text-md ">

          <div style={{ background: isActive ? "#E11D48" : "black", color: isActive ? "white" : "white",  }}
            onClick={handleClick1}
            className="PaymentOption bg-black "
          >
            CARD
          </div>
          <div style={{ background: isActive2 ? "#E11D48" : "black", color: isActive2 ? "white" : "white",
            }}
            onClick={handleClick2}
            className="PaymentOption bg-black "
          >
            CASH
          </div>
          <div style={{ background: isActive3 ? "#E11D48" : "black", color: isActive3 ? "white" : "white",
            }}
            onClick={handleClick3}
            className="PaymentOption bg-black "
          >
            UPI
          </div>
        </div>
        </div>
        <div className="book_option px-8">
          <div className="">
            <p className="dis_text1 text-sm">
              Total Price
            </p>
            <p className="font-bold text-2xl">
              Rs.{total}
            </p>
          </div>
          <Button btnText="Book Now"
            handleClick={handleClick}
            className="bg-rose-600 h-12 rounded-2xl w-28 text-white font-bold" />
        </div>
      </div>
    </div>
  )
}

export default TicketDis

