import { useFormik } from "formik";
import React, { useCallback, useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import Button from "../components/Button/Button";
import Inputbox from "../components/Input/Inputbox";
import { postApicall } from "../Network/ApiCall";
import { SEND_OTP } from "../Network/baseUrl";
import { MobileNo_Schema } from "../utils/validation";
import "./Signup.css"


export default function Signup() {
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);

  // useEffect(() => {
  //   const auth = localStorage.getItem("auth");
  //   if (auth) {
  //     navigate("/bar-list");
  //   }
  // }, []);

  const Intial = { num: '' }

  const { handleChange, handleSubmit, values, errors, touched } = useFormik({
    initialValues: Intial,
    validationSchema: MobileNo_Schema,
    onSubmit: values => {
      return (
        handleSignup(values)
      )
    }
  });

  const onChange = () => { }
  const clickHandler = (e) => {
    e.preventDefault()
    console.log(errors)
    handleSubmit();
  }

  // =============== send api calling =====================


  // console.log("data" , JSON.parse(localStorage.getItem("data")))

  const sucessSignUpCallback = useCallback(async (response) => {
    console.log("response ==", response)
    setLoading(false);
    navigate("/otp" , {
      state :  `+91${values.num}`
    });
    
  });
  const errorSignUpCallback = useCallback(async (message) => {
    setLoading(false);
  });

  const handleSignup = () => {
    if (values.num) {
      setLoading(true);
      const url = SEND_OTP;
      const params = { phoneNumber: `+91${values.num}` }
      postApicall(url, params, sucessSignUpCallback, errorSignUpCallback)
    }

  };


  return (
    <div className=" h-screen w-screen flex flex-col items-center py-10" >
      <div className=" text-7xl mb-32">KRUNOS</div>
      <div className="mb-2 font-bold">Please enter your Phone Number</div>
      <form onSubmit={clickHandler} className="flex flex-col items-center">
        <div className="mb-44 mainDiv">
          <div className="w-screen text-white flex items-center justify-center mb-2">
            <Inputbox
              name="countery"
              className="border-rose w-20 border h-12 tracking-widest text-xl bg-rose-600 rounded-lg text-center"
              value={"+91"}
              readOnly
              handleChange={onChange}
            />

            <Inputbox
              autoComplete="off"
              name="num"
              type="number"
              className="numberF border-rose w-2/2 border h-12 tracking-widest text-xl bg-rose-600 rounded-lg text-center"
              placeholder="PHONE"
              minLength={9}
              errors={errors.num && touched.num}
              handleChange={handleChange}
              value={values.num} />
          </div>
          <div className="flex w-screen items-center text-white justify-center">
            {
              errors && <span className="text-rose-600 center" style={{fontSize:"13px" , fontWeight:"700"}}>{errors.num}</span>
            }
          </div>
        </div>
        <div>
          <Button btntype="submit"
            handleClick={clickHandler}
            btnText="PROCCED"
            className="p-2 hover:font bold hover:text-black hover:bg-white w-52 bg-black text-xl text-white rounded-full shadow-lg mb-2"
            loading={loading} />

        </div>
      </form>
      <p className=" w-5/6 font-sm text-center">
        By signing up, you agree to our{" "}
        <Link to="/terms" className=" font-bold underline"> Terms. </Link>
      </p>
      <p className="e w-5/6 font-sm text-center">
        See how we use your data in our{" "}
        <Link to="/privacy" className=" font-bold underline"> Privacy Policy </Link>
      </p>
    </div>
  );
}
