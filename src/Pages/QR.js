import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setUserInfo } from "../features/userSlice";
import { db } from "../firebase";
import firebase from "firebase";
import axios from "axios";

export default function QR(x) {
  const [check, setCheck] = useState("");
  const [details, setDetails] = useState({});
  const [data, setData] = useState({});
  const [loading, setLoading] = useState(false);
  const val = JSON.parse(localStorage.getItem("userId"));
  
  useEffect(() => {
    axios
      .get(`https://theblach.com/api/user/getUser/${val}`)
      .then((res) => {
        setData(res.data);
        setLoading(true);

        // setData(res.data
      })
      .catch((err) => {
        console.log(err);
      });
    // axios.get(`https://theblach.com/api/user/getUser/${val}`)
    // .then((res)=>{
    //   setDetails(res.data)
    // })
    // .catch((err)=>{
    //   console.log(err);
    // })
  }, []);


  const handleClient = () => {
    firebase
      .firestore()
      .collection("2500")
      .doc(userId)
      .get()
      .then((snapshot) => {
        console.log(snapshot.data());
      })
      .catch((e) => console.log(e));
  };

  const dispatch = useDispatch();
  const userId = useSelector((state) => state.user.userId);
  const userName = useSelector((state) => state.user.userName);

  return (
    <div
      class="App h-screen w-screen flex flex-col items-center justify-center"
      onClick={() =>
        dispatch(
          setUserInfo({
            userId: x.Id,
            userName: x.identity,
          })
        )
      }
    >
      <div class="items-center text-center flex flex-col place-content-between rounded-lg px-2 py-5 ">
        <button className="text-white font-bold  text-3xl">QR Code</button>
        <div className="text-white/50 font-light text-xs pb-6">
          One Time Redeemable
        </div>
        {loading && (
          <>
            <img
              className="h-[200px] rounded-lg"
              src={`https://theblach.com/api/user/getQRCode/${data._id}/${data.qr[0].qrCodeId}`}
              alt=""
            />
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                margin: "10px",
                fontSize: "24px",
              }}
            >
              <p className="text-white text-2xl font-bold pt-6">{data.name}</p>
              <p className="text-white font-light pt-1 text-lg">
                {data.quantity} person(s)
              </p>
              <p className="text-white text-lg ">{data.paymentType}</p>
              <p className="text-white text-lg ">{new Date(data.createdAt).toLocaleString()}</p>
              {/* add date */}
              <p className="text-white text-lg "></p>
              <p className="text-white text-lg mb-10">Total: {data.amount} </p>

              <button className="text-white/50 mt-20 rounded-full p-3 font-bold text-7xl ">
                {data.qr[0].id}
              </button>
            </div>
          </>
        )}
      </div>

      {/*  */}
    </div>
  );
}
