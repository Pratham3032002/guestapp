import { useFormik } from "formik";
import React, { useCallback, useEffect, useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import Button from "../components/Button/Button";
import Inputbox from "../components/Input/Inputbox";
import { postApicall } from "../Network/ApiCall";
import { BaseUrl, SEND_OTP, VERIFY_OTP } from "../Network/baseUrl";
import { Otp_Schema } from "../utils/validation";
import "../Pages/Signup.css"

export default function OTP() {
  const navigate = useNavigate();
  const [num, setNum] = useState("");
  const [time, setTime] = useState(60);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [errorText, setErrorText] = useState("");
  const [inputVal, setInputVal] = useState("")
  const [apiRes, setApiRes] = useState({})
  const { state } = useLocation()

  const auth = localStorage.getItem("auth");
  useEffect(() => {
    if (auth) {
      navigate("/get-started")
    }
  })
  let number = localStorage.getItem("number");
  useEffect(() => {

    setNum(number);
    setInterval(() => {
      if (time > 0) setTime((time) => time - 1);
    }, 1000);
  }, []);

  useEffect(() => {
    if (error) {
      setTimeout(function () {
        setError(false);
      }, 2500);
    }
  });
  const handleChange = (e) => {
    var numbers = /^[0-9]+$/;
    if (e.target.value.match(numbers)) {
      setInputVal(e.target.value)
    }else{
      setInputVal(e.target.value)
    }
  }
  const sucessSignUpCallback = useCallback(async (res) => {
    setLoading(false);
    setApiRes(res.otp.updatedUser)
    if (res.otp.updatedUser.isRegistered && res.otp.updatedUser.isVerified) {
      const Data = {
        contact: state,
        email: res.otp.updatedUser.email,
        name: res.otp.updatedUser.name
      }
      localStorage.setItem("data" , JSON.stringify(Data))
      localStorage.setItem("userId" ,res.otp.updatedUser._id)
      localStorage.setItem("auth", true)
      localStorage.setItem("approved", true)
      localStorage.setItem("number", state)
      navigate("/bar-list")
    } else if (res.otp.updatedUser.isVerified) {
      localStorage.setItem("auth", true)
      localStorage.setItem("number", state)
      navigate("/get-started")
    } else if (res.opt === "The Otp is incorrect") {
      console.log("You have entered wrong OTP")
    }

    setInputVal("")


  });
  const clickHandler = (e) => {
    e.preventDefault()
    if (inputVal) {
      if (inputVal.length > 5) {
        setLoading(true);
        const url = VERIFY_OTP;
        const params = { phoneNumber: state, otp: inputVal }
        postApicall(url, params, sucessSignUpCallback, errorSignUpCallback)
      }
    } else {
      setErrorText("This feild is required")
    }
  }
  // =============== send api calling =====================

  // const sucessSignUpCallback = useCallback(async (res) => {

  //   setLoading(false);
  //   console.log("response", res)


  //   // if (state == number) {
  //   //   localStorage.setItem("auth", true)
  //   //   localStorage.setItem("approved", true)
  //   //   localStorage.setItem("number", state)
  //   //   navigate("/bar-list")
  //   // } else {
  //   //   localStorage.setItem("auth", true)
  //   //   localStorage.setItem("number", state) 
  //   //   localStorage.removeItem("data")
  //   //   localStorage.removeItem("userId")
  //   //   navigate("/get-started");
  //   // }
  //   // const MainNo = localStorage.getItem("number")


  // });

  const errorSignUpCallback = useCallback(async (message) => {
    setLoading(false);
  });

  // const handleOtp = () => {
  //   console.log(errors)
  //   setLoading(true);
  //   const url = VERIFY_OTP;
  //   const params = { phoneNumber: state, otp: values.code }
  //   console.log("params", params)
  //   postApicall(url, params, sucessSignUpCallback, errorSignUpCallback)

  // };


  const ResendOtp = async () => {
    await BaseUrl.post(SEND_OTP, { phoneNumber: num })
      .then((res) => {
        setTime(60);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <form onSubmit={clickHandler} className="h-screen w-screen flex flex-col items-center py-10" >
      <div className=" text-7xl mb-28">KRUNOS</div>
      <div className="w-2/3 text-center font-bold text-sm py-2 mb-2">
        Enter the text sent to your number {state}.{" "}
      </div>
      <div className="mb-16">
        <div className="w-screen flex text-white items-center justify-center mb-2">
          <Inputbox
            name="code"
            type="number"
            // errors={errors.code && touched.code}
            className="border-rose w-60 border h-12 tracking-widest text-xl bg-rose-600 rounded-lg text-center numberF"
            placeholder=" - - - - - - "
            handleChange={handleChange}
            value={inputVal || ""}
          />
        </div>
        <div className="flex w-screen items-center text-white justify-center">
          {/* {
            errors && <span className="text-rose-600 center">{errors.code}</span>
          } */}
        </div>
      </div>

      {time > 0 ? (
        <p className=" w-5/6 font-sm font-bold text-center mb-20">
          Resend text in {" "}
          <span className=" font-bold underline">{time}</span> s
        </p>
      ) : (
        <p className=" w-5/6 font-sm font-bold text-center mb-20 cursor-pointer" onClick={ResendOtp}>
          Resend OTP
        </p>
      )}
      <div>
        <Button btntype="submit"
          btnText="Submit OTP"
          className="p-2 hover:font bold hover:text-black hover:bg-white w-52 bg-black text-xl text-white rounded-full shadow-lg mb-2"
          loading={loading} />
        {error && (
          <p className="text-red-600 font-semibold text-center text-xl pb-3">
            Incorrect OTP
          </p>
        )}
      </div>

      <p className=" w-5/6 font-sm text-center">
        By signing up, you agree to our{" "}
        <Link to="/terms" className=" font-bold underline"> Terms. </Link>
      </p>
      <p className="e w-5/6 font-sm text-center">
        See how we use your data in our{" "}
        <Link to="/privacy" className=" font-bold underline"> Privacy Policy </Link>
      </p>
    </form>
  );
}
