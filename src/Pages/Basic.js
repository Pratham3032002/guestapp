import React, { useState } from "react";
import { db } from "../firebase";
import firebase from "firebase";
// import { QuantityPicker } from "react-qty-picker";
// import Modal from "react-modal";
import axios from "axios";
// import QRCode from "react-qr-code";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import Loader from "../components/Loader";

function loadScript(src) {
  return new Promise((resolve) => {
    const script = document.createElement("script");
    script.src = src;
    script.onload = () => {
      resolve(true);
    };
    script.onerror = () => {
      resolve(false);
    };
    document.body.appendChild(script);
  });
}

export default function Basic({ obj, setObj }) {
  const [total, setTotal] = useState();
  const [payment, setPayment] = useState();
  const [data, setData] = useState({});
  const [userId, setUserId] = useState();
  const navigate = useNavigate("");

  // useEffect(() => {
  //   const user = localStorage.getItem("userId");
  //   if (user == undefined) {
  //     navigate("/");
  //   }
  // }, []);

  useEffect(() => {
    let userId = JSON.parse(localStorage.getItem("userId"));
    setUserId(userId);
    let data = JSON.parse(localStorage.getItem("data"));
    setData(data);
  }, []);

  useEffect(() => {
    axios
      .get("http://localhost:3000/api/admin/getAll")
      .then((res) => {
        setTotal(res.data[0].basic);
        setPayment(res.data[0].basic);
      })
      .catch((err) => {});
  }, []);

  const [customerName, setCustomerName] = useState("");
  const [customerPhone, setCustomerPhone] = useState("");
  const [quantity, setQuantity] = useState(1);
  const [isActive, setIsActive] = useState(false);
  const [isActive2, setIsActive2] = useState(false);
  const [paymentCard, setPaymentCard] = useState(false);
  const [paymentCash, setPaymentCash] = useState(false);
  const [errorEmpty, setErrorEmpty] = useState("");
  // const [redirect, setRedirect] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [loading, setLoading] = useState(false);

  // function toggleModal() {
  //   setIsOpen(!isOpen);
  // }

  const onPressPlus = () => {
    setQuantity(quantity + 1);
    setTotal(total + payment);
  };

  const onPressMinus = () => {
    setQuantity(Math.max(1, quantity - 1));
    setTotal(Math.max(payment, total - payment));
  };

  const handleClick1 = () => {
    setIsActive(true);
    setIsActive2(false);

    setPaymentCard(true);
    setPaymentCash(false);
  };

  const handleClick2 = () => {
    setIsActive2(true);
    setIsActive(false);
    setPaymentCash(true);
    setPaymentCard(false);
  };
  async function displayRazorpay(userId) {
    const res = await loadScript(
      "http://checkout.razorpay.com/v1/checkout.js"
    );

    if (!res) {
      alert("Razorpay SDK failed to load. Are you online?");
      return;
    }

    const data = await fetch("http://localhost:3000/api/razorpay/order", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: JSON.stringify({
        amount: total.toString(),
        currency: "INR",
        userId,
      }),
    }).then((t) => t.json());

   

    const options = {
      key: "rzp_live_Q5cVG0uCv8ZIxY",
      currency: data.currency,
      amount: data.amount.toString(),
      order_id: data.id,
      name: "Payment",
      description: "Thank you for nothing. Please give us some money",
      image: "http://localhost:1337/logo.svg",
      handler: async function (response) {


        const data = await fetch("http://localhost:3000/api/razorpay/verify", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(response),
        }).then((t) => t.json());

        if (data.signatureIsValid === true) {

          setObj("");
          navigate("/paymentprocessing-basic-surge-ws");
        }
      },
      prefill: {
        name: data.name,
        email: data.email,
        phone_number: data.contact,
      },
    };
    const paymentObject = new window.Razorpay(options);
    paymentObject.open();
  }

  const handleCustomer = async (e) => {
    window.sessionStorage.setItem("name", customerName);

    setErrorEmpty("");
    if (paymentCard) {
      db.collection("2500").add({
        identity: customerName,
        lead: customerPhone,
        paymentMethod: "CARD",
        timestamp: firebase.firestore.FieldValue.serverTimestamp(),
        guestNum: quantity,
        amount: total,
      });
      setLoading(true);
      await axios
        .put(`http://localhost:3000/api/user/updateUser/${userId}`, {
          ...data,
          ticketType: "BASIC",
          amount: total.toString(),
          quantity: quantity.toString(),
          paymentType: "Card",
        })
        .then((res) => {
          setLoading(false);
          localStorage.setItem("userId", JSON.stringify(res.data._id));
          displayRazorpay(res.data._id);
          // setObj("");
          // navigate("/paymentprocessing-basic-surge-ws");
        })
        .catch((err) => {
          setLoading(false);
          console.log(err);
        });
      // setCustomerName('');
      // setCustomerPhone('');
      // setQuantity(1);
      // setTotal(2500);
      // setPaymentCard(false)
      // setRedirect(true)
      setIsOpen(true);
    } else if (paymentCash) {
      db.collection("2500").add({
        identity: customerName,
        lead: customerPhone,
        paymentMethod: "CASH",
        timestamp: firebase.firestore.FieldValue.serverTimestamp(),
        guestNum: quantity,
        amount: total,
      });
      setLoading(true);
      await axios
        .put(`http://localhost:3000/api/user/updateUser/${userId}`, {
          ...data,
          ticketType: obj,
          amount: total.toString(),
          quantity: quantity.toString(),
          paymentType: "Cash",
        })
        .then((res) => {
          setLoading(false);

          localStorage.setItem("userId", JSON.stringify(res.data._id));
          setObj("");
          navigate("/paymentprocessing-basic-surge-ws");
        })
        .catch((err) => {
          setLoading(false);

          console.log(err);
        });
      // setCustomerName('');
      // setCustomerPhone('');
      // setQuantity(1);
      // setTotal(2500);
      // setPaymentCash(false)
      // setRedirect(true)
      setIsOpen(true);
    }

    if (paymentCard === false && paymentCash === false) {
      setErrorEmpty("Please select atleast one payment method.");
    }
  };

  // let x;
  // if (redirect) {
  //   x = <Navigate to="/paymentprocessing-basic-surge-ws" />;
  // } else {
  //   x = "";
  // }
  return (
    <div className="App h-screen w-screen flex flex-col items-center justify-center">
      {errorEmpty && (
        <p className="text-center text-red-500 text-xs py-2">{errorEmpty}</p>
      )}

      <div className="flex w-screen items-center text-white justify-center font-extrabold mt-24 text-3xl">
        Basic Access
      </div>
      <div className="flex w-screen items-center text-white justify-center font-light text-sm pb-10">
        Rs. {payment}/person (100% Redeemable)
      </div>

      <div className="pb-12 flex w-screen flex items-center justify-center flex-col">
        <div className="w-screen text-white flex items-center justify-center">
          <input
            onChange={(e) => setCustomerName(e.target.value)}
            value={data.name}
            disabled
            className="px-2 border-rose border w-2/3 h-12 py-1 text-18 bg-black"
            placeholder="Name"
          ></input>
        </div>
        <div className="py-4 w-screen text-white flex items-center justify-center">
          <input
            onChange={(e) => setCustomerPhone(e.target.value)}
            value={data.contact}
            disabled
            className="px-2 border-rose border w-2/3 h-12 py-1 text-18 bg-black"
            placeholder="Phone Number"
          ></input>
        </div>
        <div className="flex flex-row-reverse mb-3">
          <div
            className="font-bold text-white text-lg p-5"
            onClick={onPressPlus}
          >
            +
          </div>
          <div className="font-bold text-white text-lg p-5">{quantity}</div>
          <div
            className="font-bold text-white text-lg p-5"
            onClick={onPressMinus}
          >
            -
          </div>
        </div>
        <div className="flex w-screen place-content-between gap-4 px-10 items-center h-10 mb-10 ">
          <div className="text-white font-bold text-2xl">Total Price</div>
          <div className="font-bold text-white text-2xl">Rs. {total}</div>
        </div>
      </div>

      <div className="font-bold text-gray-500 text-lg">Select a payment method</div>
      <div className="flex pt-4 pb-8 gap-4  text-lg font-bold">
        <div
          style={{
            background: isActive ? "black" : "transparent",
            color: isActive ? "white" : "gray",
          }}
          onClick={handleClick1}
          className="bg-black text-gray-500 p-2 rounded-lg"
        >
          CARD
        </div>
        <div
          style={{
            background: isActive2 ? "black" : "transparent",
            color: isActive2 ? "white" : "gray",
          }}
          onClick={handleClick2}
          className="bg-black text-gray-500 p-2 rounded-lg"
        >
          CASH
        </div>
      </div>

      {errorEmpty && <p className="text-red-500 text-xs py-2">{errorEmpty}</p>}

      <div>
        <button
          onClick={handleCustomer}
          disabled={loading}
          className="p-2 hover:font bold hover:text-black hover:bg-white w-32 bg-rose-600 text-xl text-white rounded-full shadow-lg mb-48"
        >
          {loading ? <Loader /> : "Book Now"}
        </button>
      </div>

      {/* <button onClick={toggleModal} className="text-sm text-rose-600 rounded-lg px-10 py-3 font-bold mt-3">Open Ticket</button>
       
       <div className="flex bg-black">
       <Modal
        isOpen={isOpen}
        onRequestClose={toggleModal}
        contentLabel="My dialog"
        className=""
        >
        
        <div className="font-bold">{customerName}</div>
        <div>{quantity} person(s)</div>
        <div>Rs. {total} paid.</div>
        <div>Friday, August 5th</div>

            
        <div className="flex flex-row text-center m-3">
        </div>
        <QRCode value={`${total}, ${customerName}`}/>
        <button className="text-white text-xs font-bold p-3 bg-rose-600 mt-5 rounded-full" onClick={toggleModal}>Close ticket</button>
        </Modal> */}
      {/* </div> */}
      {
        /* <div class="py-8 flex gap-4">
      <div class="bg-black text-white rounded-md p-4">CASH</div>
      <div class="bg-black text-white rounded-md p-4">CARD</div>
      </div> */
      }
    </div>
  );
}

