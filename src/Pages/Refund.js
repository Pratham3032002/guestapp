import React from 'react'

export default function Refund() {
  return (
    <div className="App w-screen flex flex-col items-center py-10">
        <div className='text-white font-bold text-4xl'>Refund Policy / Returns</div>
        <p className="text-white text-sm w-5/6 mt-5">Thank you for your purchase. We hope you are happy with your purchase. However, if you are not
completely satisfied with your purchase for any reason, you may not demand return from us Please
see below for more information on our return policy.</p>

<div className='text-white font-bold text-4xl'>Refunds</div>
<p className="text-white text-sm w-5/6 mt-5">After sending your request for entry and getting it approved, we will process not process any [return].
Please understand that if your order is still pending, then it will be refunded if entry is not approved.
Refunds may take 1-2 billing cycles to appear on your credit card statement, depending on your
credit card company. We will notify you by email when your return has been processed.</p>

<p className="text-white text-sm w-5/6 mt-5">The following items cannot be [returned]:
● If you did not get a table when it was not explicitly promised
● Once you have been approved entry, it is up to the discretion of the management
staff to grant refund.
● Refund for a missing member of your party</p>

<p className="text-white text-sm w-5/6 mt-5">Please Note
● Sale items are FINAL SALE and cannot be returned.</p>


<div className='text-white font-bold text-4xl'>Questions</div>
<p className="text-white text-sm w-5/6 mt-5">If you have any questions concerning our return policy, please contact us at:
contact@usekrunos.com</p>

    </div>
  )
}
