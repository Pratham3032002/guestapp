import { configureStore } from "@reduxjs/toolkit";
// import { persistStore, persistReducer } from "redux-persist" // imports from redux-persist
// import storage from "redux-persist/lib/storage" // defaults to localStorage for web

import reducer from "./reducer"; 

// const persistConfig = { // configuration object for redux-persist
//   key: "root",
//   storage, // define which storage to use
// }

//const persistedReducer = persistReducer(persistConfig, reducer) // create a persisted reducer

const store = configureStore({ // store configuration
  reducer
});

//export const  persistor = persistStore(store); // used to create the persisted store, persistor will be used in the next step
export default store; // used to create the persisted store, persistor will be used in the next step