/* eslint-disable linebreak-style */
import { SET_USER_DATA } from "./action";

const initialState = {

  UserData: [],
  
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
  case SET_USER_DATA:
  
    return {
      ...state,
      UserData: action.UserData
    }
  default:
    return state
  }


}

export default reducer