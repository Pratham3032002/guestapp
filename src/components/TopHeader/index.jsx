import React from 'react'
import "./style.css"
import { backImage } from '../../utils/Images'
import { useNavigate } from 'react-router-dom'

const TopHeader = ({ username }) => {
  const Navigate = useNavigate()
  const backClick =()=>{
    Navigate(-1)
  }
  return (
    <div className="back_img">
      <div className="image_back" onClick={backClick}>
        <img src={backImage} alt="" />
      </div>
      <div >
      <p className="text-right font-bold ">Hello,</p>
      <p className="text-right font-bold">{username}</p>
        
      </div>
    </div>
  )
}

export default TopHeader