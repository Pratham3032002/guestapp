export const  formatPhoneNumber = (value) => {
    // if input value is falsy eg if the user deletes the input, then just return
    if (!value) return value;
  
    // clean the input for any non-digit values.
    const phoneNumber = value.replace(/[^\d]/g, '');
  
    console.log("phoneNumber == ", phoneNumber)
    // phoneNumberLength is used to know when to apply our formatting for the phone number
    const phoneNumberLength = phoneNumber.length;
  
    // we need to return the value with no formatting if its less then four digits
    // this is to avoid weird behavior that occurs if you  format the area code to early
  
    if (phoneNumberLength < 4) return phoneNumber;
  
    // if phoneNumberLength is greater than 4 and less the 7 we start to return
    // the formatted number
    if (phoneNumberLength < 7) {
      return `(${phoneNumber.slice(0, 3)}) ${phoneNumber.slice(3)}`;
    }
  
    // finally, if the phoneNumberLength is greater then seven, we add the last
    // bit of formatting and return it.
    console.log("phoneNumber === oikkokokioko ",`(${phoneNumber.slice(0, 3)}) ${phoneNumber.slice(
      3,
      6,
    )}-${phoneNumber.slice(6, 10)}`)
    return `(${phoneNumber.slice(0, 3)}) ${phoneNumber.slice(
      3,
      6,
    )}-${phoneNumber.slice(6, 10)}`;

  }



  

  export const  formatdate = (dateStr) => {
    setdateFormate(dateStr)
    const regExp = /^(\d\d?)\/(\d\d?)\/(\d{4})$/;
    let matches = dateStr.match(regExp);
    let isValid = matches;
    let maxDate = [0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    if (matches) {
      const month = parseInt(matches[1]);
      const date = parseInt(matches[2]);
      const year = parseInt(matches[3]);

      isValid = month <= 12 && month > 0;
      isValid &= date <= maxDate[month] && date > 0;

      const leapYear = (year % 400 == 0)
        || (year % 4 == 0 && year % 100 != 0);
      isValid &= month != 2 || leapYear || date <= 28;
    }
    setisdateChecked(isValid)

  }