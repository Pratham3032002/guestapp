import React, { useState } from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
// import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter, Routes, Route, Outlet } from "react-router-dom";
import store from "../src/app/Store";
import Dashboard from "./Pages/Pages_Admin/Dashboard";
import { Provider } from "react-redux";
import Privacy from "./Pages/Privacy";
import Terms from "./Pages/Terms";
import Refund from "./Pages/Refund";
import OTP from "./Pages/OTP";
import Signup from "./Pages/Signup";
import ProtectedLayout from "./components/ProtectedLayout";
import { useEffect } from "react";
import SignUp from "./Pages/SignUpScreen/Index";
import "./utils/design.css"
import GetStarted from "./Pages/GetStarted";
import FasterAccess from "./Pages/FasterAccess";
import EnjoyInsta from "./Pages/EnjoyInsta/EnjoyInsta";
import ConfirmDetail from "./Pages/ConfirmDetail";
import WhiskyShamba from "./Pages/WhiskySamba";
import Home from "./Pages/Home";
import TicketType from "./Pages/Home/TicketType";
import TicketDis from "./Pages/Home/TicketDis";
import WaitingPage from "./Pages/Home/waitingPage";
import FeedbackPage from "./Pages/Home/FeedbackPage";
import PriveeTicket from "./Pages/Home/PriveeTicket";
import BarList from "./Pages/BarList/BarList";
import QrPage from "./Pages/QrPage/Index";

const Routing = () => {
  // const [obj, setObj] = useState("");
  const [auth, setAuth] = useState(true);
  

  // useEffect(() => {
  //   // localStorage.clear();
  //   setTimeout(() => {
  //     setAuth(false);
  //   }, 1000);
  // }, []);

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<ProtectedLayout auth={auth}>  <Outlet /> </ProtectedLayout>} >
          <Route path="get-started" element={<GetStarted />} />
          <Route path="faster-access" element={<FasterAccess />} />
          <Route path="enjoy-instant" element={<EnjoyInsta />} />
          <Route path="confirm-your-detail" element={<ConfirmDetail />} />
          <Route path="qr-page" element={<QrPage />} />
          <Route exact path="/bar-list/whisky-samba/:id" element={<WhiskyShamba />} />
          <Route exact path="bar-list" element={<BarList />} />
          <Route exact path="/bar-list/Home/:id" element={<Home />} />
          <Route exact path="ticket-type" element={<TicketType />} />
          <Route exact path="ticket-dis" element={<TicketDis />} />
          <Route exact path="waiting-page" element={<WaitingPage />} />
          <Route exact path="feedback-page" element={<FeedbackPage />} />
          <Route exact path="qr-page/privee-page" element={<PriveeTicket />} />
        </Route>
        <Route path="/admin" element={<Dashboard />} />
        <Route path="/privacy" element={<Privacy />} />
        <Route path="/terms" element={<Terms />} />
        <Route path="/refund" element={<Refund />} />
        <Route path="/otp" element={<OTP />} />
        <Route path="/signup" element={<Signup />} />
        <Route index element={<SignUp />} />
        
      </Routes>
    </BrowserRouter>
  );
};

const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(
  <Provider store={store}>
    <Routing />
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
