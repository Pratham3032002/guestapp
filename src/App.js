import logo from "./logo.svg";
import "./App.css";
import {
  Link,
  useNavigate,
} from "react-router-dom";

import { Input, stringify } from "postcss";
import React, { useState } from "react";
import styled from "styled-components";
import img from "./../src/assets/img.jpg";
import { useEffect } from "react";
import axios from "axios";
import { BaseUrl, GET_ALL } from "./Network/baseUrl";

function App({ setObj, obj }) {
  const [data, setData] = useState([]);
  const navigate = useNavigate();

  // useEffect(() => {
  //   const user = localStorage.getItem("userId");
  //   if (user == undefined) {
  //     console.log("user", user);
  //     navigate("/");
  //   }

  // }, []);


  useEffect(() => {
    BaseUrl.get(GET_ALL)
      .then((res) => {
        setData(res.data);
      })
      .catch((err) => {});
  }, []);
  const inputHandler = (val) => {
    console.log("val == ", val)
    setObj(val);
  };

  let msg1 = "VIP Tables Get Red Carpet Entry";
  let msg2 = "Complimentary Drink";
  let msg3 = "Instant Table & Bar Access";

  return (
    <div className="App h-screen w-screen">
      <div className="w-screen flex flex-flow flex-col items-center justify-center">
        <img
          className="mt-5 px-2 border border-gray-300"
          src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfMEaWXglwsSBF1gPNYIeZ2eIok5aA-S1oABhYU5AOgA&s"
          alt=""
        ></img>
        <div className="-mt-10">
          <div className="row">
            <div className="col-md-6">
              <div className="d-flex justify-content-between align-items-center breaking-news">
                <marquee
                  width="130"
                  className="news-scroll"
                  behavior="scroll"
                  direction="left"
                  // onmouseover="this.stop();"
                  // onmouseout="this.start();"
                >
                  {" "}
                  <a href="#" className="text-white font-light text-sm">
                    {msg1}
                  </a>{" "}
                  <span className="px-4"> </span>{" "}
                  <a href="#" className="text-white font-light text-sm">
                    {msg2}{" "}
                  </a>{" "}
                  <span className="px-4"></span>{" "}
                  <a href="#" className="text-white font-light text-sm">
                    {msg3}{" "}
                  </a>
                </marquee>
              </div>
            </div>
          </div>
        </div>

        <div className="pt-16">
          <div className=" px-8 items-center font-bold text-xl justify-center w-screen">
            <div className=" h-52 relative">
              <img
                className="rounded-xl mix-blend w-full h-full object-cover absolute"
                src={img}
              />
              <div className="h-52 bg-black/50 absolute w-full"></div>

              <div className="object-cover w-full h-full flex flex-col justify-center items-center absolute">
                <span className=" text-white text-4xl font-medium ">
                  VIP Fast Track
                </span>
                <p className="text-white text-xs text-center font-light p-1">
                  Instant & exclusive table access
                </p>

                <div
                  className="w-2/3 mt-4 p-2 text-center bg-rose-600 rounded-xl font-bold text-xl shadow-lg text-white"
                  onClick={() => inputHandler("VIP")}
                >
                  <Link to="/vipline-surge-ws">
                    Rs. {data.length > 0 && data[0].VIP}
                  </Link>
                </div>
              </div>
            </div>

            {/* Instant entry, and gauranteed access to an exclusive table with wide range of liquor. Whisky Samba VIP perks, and a curated experience. 100% redeemable on anything in the menu. */}

            <div className="h-52 relative mt-10 rounde">
              <img className="rounded-xl mix-blend w-full h-full object-cover absolute"
                src="https://b.zmtcdn.com/data/reviews_photos/3e7/d18b07501d0ab503d0bd9dc659ec83e7_1590941272.jpg?fit=around|771.75:416.25&crop=771.75:416.25;*,*"
              />
              <div className="h-52 bg-black/50 absolute w-full "></div>
              <div className="object-cover w-full h-full flex flex-col justify-center items-center absolute">
                <span className="text-white text-4xl font-medium">
                  Basic Access
                </span>
                <p className="text-white text-xs text-center font-light p-1">
                  Bar access, subject to waiting.
                </p>

                <div
                  className="w-2/3 mt-4 p-2 text-center bg-white rounded-xl font-bold text-xl shadow-lg text-black"
                  onClick={() => inputHandler("BASIC")}
                >
                  <Link to="/basic-surge-ws">
                    Rs. {data.length > 0 && data[0].basic}
                  </Link>
                </div>
              </div>
            </div>

            <div className="text-center mt-2">
              <span className="text-white text-center text-xs font-light ">
                *100% Redeemable for anything on the menu*
              </span>
            </div>
            {/*  */}
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
